require 'spec_helper'

describe 'motd' do

  context 'supported osfamily' do
    let(:facts) { { osfamily: 'Debian' } }

    context 'with defaults' do
      it { should contain_file('/etc/motd') }
    end

    context 'with source param' do
    end

    context 'with content param' do
    end
  end

  context 'unsupported osfamily' do
  end

end
